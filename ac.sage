# Copyright (C) 2022 Dominic Walden

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.

import copy

from sage.symbolic.operators import add_vararg, mul_vararg

class CombinatorialClass(SageObject):

    def __neg__(self):
        x = copy.deepcopy(self)
        x.gf = -x.gf
        return x

    def __add__(self, y):
        return Union(self, y)

    def __radd__(self, x):
        return Union(x, self)

    def __sub__(self, y):
        return Union(self, y, '-')

    def __rsub__(self, x):
        return Union(x, self, '-')

    def __mul__(self, y):
        return Product(self, y)

    def __rmul__(self, x):
        return Product(x, self)

    def __div__(self, y):
        return Product(self, y, '/')

    def __rtruediv__(self, x):
        return Product(x, self, '/')

    def __pow__(self, y):
        return Power(self, y)

    def __rpow__(self, x):
        return Power(x, self)

    def __init__(self, specification, cls):
        self.cls = cls
        self.specification = specification
        # TODO: when this has multiple solutions
        self.gf = solve([f == specification], cls, solution_dict=True)[0][cls]

    def plot(self):
        complex_plot(self.gf, (-2, 2), (-2, 2))

    def dominant_poles(self):
        # Works out poles by finding roots of the denominator
        # Assumes f is rational, can we check this?
        try:
            singularities = self.gf.denominator().roots()
        except:
            singularities = self.gf.denominator().roots(ring=CDF)
        # smallest singularity
        sings_temp = []
        for singularity in singularities:
            sings_temp.append(abs(singularity[0]))
        minimum = min(sings_temp)
        dominant_poles = [x for x in singularities if abs(x[0]) == minimum]
        return dominant_poles

    def principle_part(self):
        # Ref: Wilf "generatingfunctionology" pg. 186-189
        # Assumes f is rational, can we check this?
        p_part = 0
        for singularity in self.dominant_poles():
            value = singularity[0]
            order = singularity[1]
            for n in range(0, order):
                h(z) = (z - value)^n * self.gf(z)
                p_part = p_part + h.maxima_methods().residue(z, value) / (z - value)^(n+1)
        return p_part

    def solutions(self, equation, z):
        solutions = []
        try:
            slns = solve(equation, z, solution_dict=True)
            for sln in slns:
                solutions.append(sln[z])
        except:
            # Fail silently
            foo = "foo"
        try:
            slns = solve(equation, z, solution_dict=True, algorithm='sympy')
            for sln in slns.args[0]:
                solutions.append(sln)
        except:
            # Fail silently
            foo = "foo"
        # Use set() to de-duplicate this
        return list(set(solutions))

    def asymptotic(self):
        # Trial-and-error
        rtn = []
        # First, try asymptotics based on poles
        try:
            print("Trying pole asymptotics")
            f = self.pole_asymptotic()
            rtn.extend(f)
        except:
            print("Failed pole asymptotics")

        # Then, try saddle-point
        try:
            print("Trying saddle-point asymptotics")
            f = self.saddle_point_asymptotic()
            rtn.extend(f)
        except:
            print("Failed saddle-point asymptotics")

        # Finally, try Hayman's method
        try:
            print("Trying Hayman's method")
            f = self.hayman_asymptotic()
            rtn.extend(f)
        except:
            print("Failed Hayman's method")

        return rtn

    def pole_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/ACqa3.pdf pg. 8
        rtn = []
        for dominant_pole in self.dominant_poles():
            p = dominant_pole[0]
            m = dominant_pole[1]
            gf = copy.deepcopy(self.gf)
            f(z) = gf.numerator()
            g(z) = gf.denominator()
            gm(z) = diff(g(z), z, m)
            rtn.append(((-1)^m * m * f(p) * (1/p)^n * n^(m-1)) / (p^m * gm(p)))
        return rtn

    def singularity_analysis_asymptotic(self):
        # Ref: AC pg. 392-395
        if self.standard_scale_asymptotic():
            # Does it already have a representation in standard scale?
            return [True, self, self.standard_scale_asymptotic()]
        elif hasattr(self, 'x') and hasattr(self, 'y'):
            # Otherwise, try to extract any sub-expression which can be
            # represented in the standard scale
            left = self.x.singularity_analysis_asymptotic()
            right = self.y.singularity_analysis_asymptotic()
            if left and left[0]:
                g(z) = self.gf * 1/left[1].gf
                stnd = left[2]
            elif right and right[0]:
                g(z) = self.gf * 1/right[1].gf
                stnd = right[2]
            else:
                g(z) = self.gf
                stnd = 1
            # Take the first expression in the Taylor expansion at 1.
            # If applicable, multiply it by the standard scale
            # representation of any sub-expression found above.
            f(n) = taylor(g(z), z, 1, 0) * stnd
            return f

    def invertible_tree(self, phi, l):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 41
        phip(z) = diff(phi(z), z)
        phipp(z) = diff(phi(z), z, 2)
        f(n) = 1 / sqrt(2 * pi * phipp(l)/phi(l)) * phip(l)^n * n^(-3/2)
        return f(n)

    def invertible_tree_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 40-43
        phi(f) = self.specification/z
        rtn = []
        u = var('u')
        solutions = solve([phi(u) == u * diff(phi(u), u)], u, solution_dict=True)
        for solution in solutions:
            rtn.append(self.invertible_tree(phi, solution[u]))
        return rtn

    def implicit_tree(self, phi, r, s):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC07-SAapps.pdf pg. 41
        phiz(z, w) = diff(phi(z, w), z)
        phiw(z, w) = diff(phi(z, w), w)
        phiww(z, w) = diff(phi(z, w), w, 2)
        a = sqrt((2 * r * phiz(r, s)) / phiww(r, s))
        f(n) = (a / (2 * sqrt(pi))) * (1/r)^n * n^(-3/2)
        return f(n)

    def implicit_tree_asymptotic(self, phi):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC07-SAapps.pdf pg. 40-41
        phiw(z, w) = diff(phi(z, w), w)
        r, s = var('r,s')
        solutions = solve([phi(r, s) == s, phiw(r, s) == 1], r, s, solution_dict=True)
        rtn = []
        for solution in solutions:
            r = real_part(solution[r]).simplify()
            s = real_part(solution[s]).simplify()
            rtn.append(self.implicit_tree(phi, r, s))
        return rtn

    def saddle_point_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC08-Saddle.pdf pg. 25
        n, z = var('n,z')
        # g(z)
        g(z) = log(self.gf(z)) - (n + 1) * log(z)
        # First derivative of g(z)
        gp(z) = diff(g(z), z)
        # Second derivative of g(z)
        gpp(z) = diff(g(z), z, 2)
        # root(s) of the saddle-point equation
        # Old: gp(z).roots(x=z)
        sp_roots = self.solutions([gp(z) == 0], z)
        rtn = []
        for root in sp_roots:
            try:
                f = self.gf(root) / (root^(n+1) * sqrt(2 * pi * gpp(root)))
                rtn.append(f)
            except:
                continue
        return rtn

    def hayman_asymptotic(self):
        # Ref: Wilf "generatingfunctionology" pg. 198
        n, z = var('n,z')
        a(r) = r * diff(self.gf(r), r) / self.gf(r)
        b(r) = r * diff(a(r), r)
        solutions = self.solutions([a(z) == n], z)
        rtn = []
        for rn in solutions:
            try:
                f = self.gf(rn) / (rn^n * sqrt(2 * pi * b(rn)))
                rtn.append(f)
            except:
                continue
        return rtn

    def test_taylor(self, g):
        for i in range(1, 11):
            print("{}: {}".format(i, numerical_approx(g(z).series(z, 10).coefficient(z, i) / self.gf(z).series(z, 10).coefficient(z, i))))
        print("{}: {}".format(100, numerical_approx(g(z).series(z, 100).coefficient(z, 100) / self.gf(z).series(z, 100).coefficient(z, 100))))
        print("{}: {}".format(1000, numerical_approx(g(z).series(z, 1000).coefficient(z, 1000) / self.gf(z).series(z, 1000).coefficient(z, 1000))))

    def test_asymptotic(self, g, rng=range(1, 11) + [100, 1000]):
        if not hasattr(self, 'series') or len(self.series) < max(rng) + 2:
            self.series = self.gf(z).series(z, max(rng) + 1)
        for i in rng:
            if self.series.coefficient(z, i) != 0:
                print("{}: {}".format(i, numerical_approx(g(i) / self.series.coefficient(z, i))))
            else:
                print("{}: {} {}".format(i, numerical_approx(g(i)), self.series.coefficient(z, i)))

class Z(CombinatorialClass):

    def __init__(self):
        z = var('z')
        self.gf = z

class Union(CombinatorialClass):

    # Schema: x + y
    def __init__(self, x, y, op='+'):
        self.x = x
        self.y = y
        cx = copy.deepcopy(x)
        if isinstance(x, CombinatorialClass):
            xgf = cx.gf
        else:
            xgf = cx
        cy = copy.deepcopy(y)
        if isinstance(y, CombinatorialClass):
            ygf = cy.gf
        else:
            ygf = cy
        if op == '-':
            self.gf = xgf - ygf
        else:
            self.gf = xgf + ygf

class Product(CombinatorialClass):

    # Schema: x * y
    def __init__(self, x, y, op='+', recursive=False):
        self.x = x
        self.y = y
        cx = copy.deepcopy(x)
        if isinstance(x, CombinatorialClass):
            xgf = cx.gf
        else:
            xgf = cx
        cy = copy.deepcopy(y)
        if isinstance(y, CombinatorialClass):
            ygf = cy.gf
        else:
            ygf = cy
        if op == '/':
            self.gf = xgf / ygf
        else:
            self.gf = xgf * ygf

        if recursive:
            super(Product, self).__init__(self.gf, f)

    def asymptotic(self):
        # Trial-and-error
        rtn = []
        # First, try invertible tree
        try:
            print("Trying invertible tree asymptotics")
            f = self.invertible_tree_asymptotic()
            rtn.extend(f)
        except:
            print("Failed invertible tree asymptotics")

        # Second, try standard scale
        try:
            print("Trying standard scale asymptotics")
            f = self.standard_scale_asymptotic()
            if f:
                rtn.append(f)
        except:
            print("Failed standard scale asymptotics")

        # Otherwise, try the other methods
        rtn.extend(super(CombinatorialClass, self).asymptotic())
        return rtn

    def invertible_tree(self, phi, l):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 41
        phip(z) = diff(phi(z), z)
        phipp(z) = diff(phi(z), z, 2)
        f(n) = 1 / sqrt(2 * pi * phipp(l)/phi(l)) * phip(l)^n * n^(-3/2)
        return f(n)

    def invertible_tree_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 40-43
        phi(f) = self.y.gf
        rtn = []
        u = var('u')
        solutions = solve([phi(u) == u * diff(phi(u), u)], u, solution_dict=True)
        for solution in solutions:
            rtn.append(self.invertible_tree(phi, solution[u]))
        return rtn

    def standard_scale_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 17
        if isinstance(self.x, Power):
            left(n) = self.x.standard_scale_asymptotic()
            if left is not None:
                if isinstance(self.y, Power):
                    b = self.y.a
                    foo = self.y.z
                else:
                    b = 1
                    foo = self.y
                if (isinstance(foo, Product) and isinstance(foo.y, Cycle)) or isinstance(foo, Cycle):
                    right(n) = log(n)^b
                    return left(n) * right(n)
        else:
            return False

class Power(CombinatorialClass):

    # Schema: z^a
    def __init__(self, z, a):
        self.z = z
        self.a = a
        cz = copy.deepcopy(z)
        if isinstance(z, CombinatorialClass):
            zgf = cz.gf
        else:
            zgf = cz
        ca = copy.deepcopy(a)
        if isinstance(a, CombinatorialClass):
            agf = ca.gf
        else:
            agf = ca
        self.gf = zgf^agf

    def asymptotic(self):
        # Trial-and-error
        rtn = []
        # First, try standard scale
        try:
            print("Trying standard scale asymptotics")
            f = self.standard_scale_asymptotic()
            if f:
                rtn.extend(f)
        except:
            print("Failed standard scale asymptotics")

        # Otherwise, try the other methods
        rtn.extend(super(CombinatorialClass, self).asymptotic())
        return rtn

    def standard_scale_asymptotic(self):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 14
        # is there a logarithmic factor?
        # I have seen two different versions of this, see ibid. pg. 28
        # See also AC pg. 441 TODO: implement (1 - z/p)^a
        # If a is not a positive integer
        if isinstance(self.z, Union) and (not isinstance(self.a, Integer) or self.a < 0):
            f(n) = n^(-self.a-1) / gamma(-self.a)
            return f(n)
        else:
            return False

class UnaryCombinatorialClass(CombinatorialClass):

    def __init__(self, z, include=None, exclude=None):
        self.z = z
        if isinstance(z, CombinatorialClass):
            zgf = z.gf
        else:
            zgf = z

        if include is None and exclude is None:
            self.gf = self.all_gf(zgf)

        if include is not None:
            if isinstance(include, list):
                rtn = 0
                for i in include:
                    rtn = rtn + self.single_gf(zgf, i)
                self.gf = rtn
            else:
                self.gf = self.single_gf(zgf, include)

        if exclude is not None:
            if isinstance(exclude, list):
                rtn = self.all_gf(zgf)
                for i in exclude:
                    rtn = rtn - self.single_gf(zgf, i)
                self.gf = rtn
            else:
                self.gf = self.all_gf(zgf) - self.single_gf(zgf, exclude)

class Sequence(UnaryCombinatorialClass):

    def all_gf(self, z):
        return 1 / (1 - z)

    def single_gf(self, z, n):
        return z^n

class Cycle(UnaryCombinatorialClass):

    # Schema: a * log(...) + b
    def __init__(self, z, include=None, exclude=None):
        super(Cycle, self).__init__(z, include, exclude)
        self.a = 1
        # TODO: Implement this properly. This also affects b.
        self.p = 1
        if isinstance(z, CombinatorialClass):
            zgf = z.gf
        else:
            zgf = z

        if not include and not exclude:
            self.l = self.all_gf(zgf)
            self.b = 0

        if include is not None:
            self.l = None
            self.b = None

        if exclude is not None:
            self.l = self.all_gf(zgf)
            if isinstance(exclude, list):
                self.b = 0
                for i in exclude:
                    self.b = self.b - self.single_gf(self.p, i)
            else:
                self.b = -self.single_gf(self.p, exclude)

    def all_gf(self, z):
        return log(1 / (1 - z))

    def single_gf(self, z, n):
        if n > 0:
            return z^n/n
        else:
            return 1

# Undirected Cycle
# Ref: https://ac.cs.princeton.edu/online/slides/AC07-SAapps.pdf pg. 24
#      "Analytic Combinatorics" pg. 133
class UCycle(Cycle):

    def __init__(self, z, include=None, exclude=None):
        super(UCycle, self).__init__(z, include, exclude)
        self.a = 1/2
        self.b = self.b * self.a
        self.gf = self.gf * self.a

class Set(UnaryCombinatorialClass):

    def all_gf(self, z):
        return e^z

    def single_gf(self, z, n):
        return z^n/factorial(n)

    def asymptotic(self):
        # Trial-and-error
        rtn = []
        # First, try explog method
        try:
            print("Trying explog asymptotics")
            f = self.explog_asymptotic()
            if f:
                rtn.append(f)
        except:
            print("Failed explog asymptotics")

        # Otherwise, try the other methods
        rtn.extend(super(UnaryCombinatorialClass, self).asymptotic())
        return rtn

    def explog(self, a, b, p):
        # Ref: https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 37
        f(n) = (e^b / gamma(a)) * (1/p)^n * n^(a-1)
        return f(n)

    def explog_asymptotic(self):
        if isinstance(self.z, Cycle):
            f(n) = self.explog(self.z.a, self.z.b, self.z.p)
            return f(n)
        else:
            return False

# class MultiSet(CombinatorialClass):

# class PowerSet(CombinatorialClass):
