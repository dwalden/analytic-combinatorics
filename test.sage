# Copyright (C) 2022 Dominic Walden
# Copyright (C) 1991--2013 by INRIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.

# This is a modified version of the test code for gdev (part of
# Algolib) which was originally licensed under the LGPLv3.

load("test_functions.sage")

f, z = var('f,z')

###########################################################################
#                        Rational functions
###########################################################################
# Denumerants
# test_pole(1/mul(x-i,i=1..10))

test_gf(Product(Power(Sequence(z), 6), Sequence(z^6)).gf, 1/(1-z)^6/(1-z^6))
test_pole(Product(Power(Sequence(z), 6), Sequence(z^6)))

test_gf(Product(Union(1, -z), 1/(1-2*z+z^4)).gf, (1-z)/(1-2*z+z^4))
test_pole(Product(Union(1, -z), 1/(1-2*z+z^4)))

test_gf(Product((1+z^2)*(1+z+z^2)*(1-z+z^2)*(1-z^2+z^4), 1/(1-z-z^3-z^5-z^7-z^9-z^11)).gf, (1+z^2)*(1+z+z^2)*(1-z+z^2)*(1-z^2+z^4)/(1-z-z^3-z^5-z^7-z^9-z^11))
test_pole(Product((1+z^2)*(1+z+z^2)*(1-z+z^2)*(1-z^2+z^4), 1/(1-z-z^3-z^5-z^7-z^9-z^11)))

# Gambler's ruin
pp=1/2
q=1-pp
k=5
c=10
l=(1+sqrt(1-4*pp*q*z^2))/2/pp/z
m=(1-sqrt(1-4*pp*q*z^2))/2/pp/z
a=(q/pp)^k*(l^(c-k)-m^(c-k))/(l^c-m^c)
test_gf(Product((q/pp)^k*(l^(c-k)-m^(c-k)), 1/(l^c-m^c)).gf, (q/pp)^k*(l^(c-k)-m^(c-k))/(l^c-m^c))
test_pole(Product((q/pp)^k*(l^(c-k)-m^(c-k)), 1/(l^c-m^c)), range(1, 21) + [99, 999])

###########################################################################
#                      Standard scale asymptotics
###########################################################################
test_standard_scale(Product(Power(Union(1, -z), -1), Cycle(z)))
test_standard_scale(Power(Union(1, Z()), 1/2))
test_standard_scale(Power(Union(1, Z()), -3/2))

###########################################################################
#                    Singularity analysis asymptotics
###########################################################################
test_singularity_analysis(Product(Power(e, -z/2 - z^2/4), Power(Union(1,  -z), -1/2)))

###########################################################################
#                        Invertible tree classes
###########################################################################
# Rooted, ordered trees
test_gf(Product(z, Sequence(f)).gf, z / (1 - f))
test_invertible_tree(Product(z, Sequence(f), True))

# From https://ac.cs.princeton.edu/online/slides/AC07-SAapps.pdf pg. 41
# phi(z, w) = z + w^2
# test_implicit_tree(z + z * f^2, phi)

###########################################################################
#                            Exp-log classes
###########################################################################
# permutations
test_exp_log(Set(Cycle(Z())))
# derangements
test_exp_log(Set(Cycle(Z(), exclude=0)))
# permutations with no cycle lengths 3, 7, 9
test_exp_log(Set(Cycle(Z(), exclude=[3, 7, 9])))
# 2-regular graphs
test_exp_log(Set(UCycle(Z(), exclude=[1, 2])))

###########################################################################
#                         Entire functions
###########################################################################
# Stirling's formula
test_gf(Set(z).gf, e^z)
test_saddle_point(Set(z))

test_gf(Product(z, Set(z)).gf, z * e^z)
test_saddle_point(Product(z, Set(z)))

# Involutions
test_gf(Set(Union(z, z^2/2)).gf, e^(z + z^2/2))
test_saddle_point(Set(Union(z, z^2/2)))

###########################################################################
#                       Finite saddle points
###########################################################################
# For some reason, this takes a long time to run
test_gf(Set(Sequence(z)).gf, exp(1/(1-z)))
test_saddle_point(Set(Sequence(z)), rng=range(1, 21))

test_gf(Set(Product(z, Sequence(z))).gf, e^(z/(1-z)))
test_saddle_point(Set(Product(z, Sequence(z))))

test_gf(Set(Product(z, Power(Sequence(z), 2))).gf, exp(z/(1-z)^2))
test_saddle_point(Set(Product(z, Power(Sequence(z), 2))))

test_gf(Product(Product(Set(Sequence(z)), Cycle(z)), Power(Union(1, -z), -1/2)).gf, e^(1/(1-z))*log(1/(1-z))/sqrt(1-z))
# test_saddle_point(Product(Product(Set(Sequence(z)), Cycle(z)), Power(Union(1, -z), -1/2)))

test_gf(Set(1/(1 - z - z^5)).gf, e^(1/(1-z-z^5)))
# test_saddle_point(Set(1/(1 - z - z^5)))

###########################################################################
#                            Hayman's method
###########################################################################
# Involutions
test_gf(Set(Cycle(z, include=[1, 2])).gf, e^(z + 1/2*z^2))
test_hayman(Set(Cycle(z, include=[1, 2])))

test_gf(Set(Power(Union(1, -z), -1/2)).gf, e^(1/sqrt(1-z)))
test_hayman(Set(Power(Union(1, -z), -1/2)), rng=range(1, 21))


###########################################################################
#                              Symbolic
###########################################################################
test_gf((1/(1-Z())^6/(1-Z()^6)).gf, 1/(1-z)^6/(1-z^6))
test_pole(1/(1-Z())^6/(1-Z()^6))

test_pole((1-Z())/(1-2*Z()+Z()^4))
