# Copyright (C) 2022 Dominic Walden

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program. If not, see
# <https://www.gnu.org/licenses/>.

def test_gf(f, g):
    if f != g:
        print("FAIL: GFs do not match: {} {}".format(f, g))
    else:
        print("PASS: GFs match")

def test_estimates(g, estimates, rng=None):
    print("Testing {}:".format(g.gf))
    i = 1
    for asym_est in estimates:
        print("Estimate {}:".format(i))
        print(asym_est)
        f(n) = asym_est
        try:
            if rng is not None:
                g.test_asymptotic(f, rng)
            else:
                g.test_asymptotic(f)
        except Exception as err:
            print("ERROR: {}".format(err))
            i = i + 1
            continue
        i = i + 1

def test_saddle_point(g, rng=None):
    test_estimates(g, g.saddle_point_asymptotic(), rng)

def test_pole(g, rng=None):
    test_estimates(g, g.pole_asymptotic(), rng)

def test_standard_scale(g, rng=None):
    test_estimates(g, [g.standard_scale_asymptotic()], rng)

def test_singularity_analysis(g, rng=None):
    test_estimates(g, [g.singularity_analysis_asymptotic()], rng)

def test_invertible_tree(g, rng=None):
    test_estimates(g, g.invertible_tree_asymptotic(), rng)

def test_implicit_tree(g, phi, rng=None):
    test_estimates(g, g.implicit_tree_asymptotic(phi), rng)

def test_exp_log(g, rng=None):
    test_estimates(g, [g.explog_asymptotic()], rng)

def test_hayman(g, rng=None):
    test_estimates(g, g.hayman_asymptotic(), rng)

def test_asymptotic(g, rng=None):
    test_estimates(g, g.asymptotic(), rng)
