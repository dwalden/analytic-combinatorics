# Analytic Combinatorics

Analytic combinatorics in Sagemath.

Based on the field of analytic combinatorics as presented by Flajolet
and Sedgewick in the book "Analytic Combinatorics" (available
http://algo.inria.fr/flajolet/Publications/AnaCombi/anacombi.html) and
associated course https://ac.cs.princeton.edu/home/

Also to a lesser extent the book "An Introduction to the Analysis of
Algorithms" by the same authors and associated course
https://aofa.cs.princeton.edu/home/

Some material is also taken from Wilf's "generatingfunctionology"
(available https://www2.math.upenn.edu/~wilf/DownldGF.html)

## How to use it

In sagemath, run: `load("ac.sage")`

Work out what combinatorial class you want to count and how to specify
it. Explanations of how to specify a class are given in the above
books and courses by Flajolet and Sedgewick. In particular, lectures 1
and 2 of the Analytic Combinatorics course.

Lets say I am interested in knowing how many trees there are with `n`
nodes. The specification for this class is: `F == z * SEQ(F)`[1]

I then translate the specification into sagemath using the functions
provided.

For example:

First, define `f` and `z` by running: `f, z = var('f,z')`

Then run: `trees = Product(z, Sequence(f), True)`

(It is recommended to always use `f` for the name of the class and `z`
for the variable. Otherwise, some functionality will not work properly.)

The generating function of the class should be available by running:
`trees.gf(z)`

There are a few functions which will attempt to find an asymptotic
estimate of the number of objects of size `n` in your class. It is up
to you to work out which function is appropriate for your class.

In the example above, it is an "invertible tree" type class and so the
function `invertible_tree_asymptotic` should be used.

Running: `trees.invertible_tree_asymptotic()`

Returns: `[1/4*4^n/(sqrt(pi)*n^(3/2))]`

(It is a list because sometimes multiple answers are found, in which
case you have to choose the right one, possibly through
trial-and-error.)

`asym_est(n) = trees.invertible_tree_asymptotic()[0]`

So, if you want to know an estimate of the number of trees with 10
nodes, you can now run: `asym_est(10)` (actually, I would recommend
instead running `numerical_approx(asym_est(10))`).

We can also test the asymptotic estimate more systematically by
running: `trees.test_asymptotic(asym_est)`

Which outputs:

	1: 0.564189583547756
	2: 0.797884560802865
	3: 0.868626687827413
	4: 0.902703333676410
	5: 0.922745608053087
	6: 0.935941822906475
	7: 0.945287729306811
	8: 0.952253834804352
	9: 0.957646427023718
	10: 0.961944533740749
	100: 0.996244522479127
	1000: 0.999624945303713

The number before the colon is `n` and the number after the colon is
the ratio of the estimate and the real answer (worked out using the
`taylor` function).

The number on the right gets closer to 1 as `n` gets higher, which is
what we want as it means the estimate is getting better as `n` gets
bigger.

## Running checks

	load("ac.sage")
	load("test.sage")

## Notes

1. This example comes from https://ac.cs.princeton.edu/online/slides/AC06-SA.pdf pg. 42 and represents the Catalan numbers https://oeis.org/A000108
